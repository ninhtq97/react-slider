import React, { useState } from 'react';
import styled from 'styled-components';

const sliderThumbStyles = props => `
  width: 25px;
  height: 25px;
  background: ${props.color};
  cursor: pointer;
  outline: 5px solid #333;
  opacity: ${props.opacity};
  -webkit-transition:.2s;
  transition: opacity .2s;
`;

const Styles = styled.div`
  display: flex;
  align-items: center;
  color: #888;
  margin-top: 2em;
  margin-bottom: 2em;

  .value {
    flex: 1;
    font-size: 2em;
  }

  .slider {
    flex: 6;
    -webkit-appearance: none;
    width: 100%;
    height: 15px;
    border-radius: 5px;
    background: #efefef;
    outline: none;

    &::-webkit-slider-thumb {
      -webkit-appearance: none;
      appearance: none;
      ${props => sliderThumbStyles(props)}
    }

    &::-moz-range-thumb {
      ${props => sliderThumbStyles(props)}
    }
  }
`;

const Slider = props => {
  const [value, setValue] = useState(0);

  const handleOnChange = e => {
    const { value } = e.target;

    const { handleUpdateColor, hexColor } = props;

    handleUpdateColor(hexColor, value);
    setValue(value);
  };

  return (
    <Styles color={props.color} opacity={value > 10 ? value / 255 : 0.1}>
      <input
        type="range"
        min={0}
        max={255}
        value={value}
        className="slider"
        onChange={handleOnChange}
      />
      <div className="value">{value}</div>
    </Styles>
  );
};

export default Slider;
